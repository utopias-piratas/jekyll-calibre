require 'calibre'

# Monkeypatch to Jekyll::Site
module Jekyll
  class Site
    alias_method :read_one, :read

    # The goal here is to create books into posts without having to
    # write them as actual files.
    def read
      # Read other posts normally
      read_one

      # Get the calibre dir from _config.yml
      calibre_dir = config.dig('calibre','dir')
      # Where to copy the covers
      cover_dir   = config.dig('calibre', 'cover', 'dir') || 'covers'

      # TODO fail if calibre_dir is missing

      # Open the database
      Calibre.db = File.join(calibre_dir, 'metadata.db')

      # For every book
      # TODO make the query customizable
      Calibre::Book.all.each do |book|
        # Create a new document on the posts collection
        # TODO use a _books collection
        post = Jekyll::Document.new('unneeded', { site: self, collection: posts })

        # The layout comes from the config
        post.data['layout'] = config.dig('calibre', 'layout') || 'post'
        # It's a markdown file
        post.data['ext'] = 'markdown'
        # The slug is the slugified title
        post.data['slug'] = Utils.slugify(book.title)
        # The title is the title :P
        post.data['title'] = book.title
        # Other metadata
        post.data['date']      = book.timestamp
        post.data['pubdate']   = book.pubdate
        post.data['author']    = book.authors.map(&:name).compact
        post.data['publisher'] = book.publishers.map(&:name).compact
        # The content comes in html but we don't care much because
        # markdown supports html
        # TODO convert to markdown anyway in case the parser gives
        # custom attributes and classes, like id's to h* tags
        post.content = book.comments.map(&:text).compact.join

        # The formats and files for easy access
        # TODO fail if missing
        files_dest = config.dig('calibre', 'files', 'dir') || 'files'
        post.data['files'] = Hash[book.data.map do |f|
          # Copy the actual files
          file      = File.join(files_dest, f.path)
          file_dest = File.join(dest, file)
          file_orig = File.join(calibre_dir, f.path)
          FileUtils.mkdir_p(File.dirname(file_dest))
          FileUtils.cp(file_orig, file_dest)
          self.keep_files << file

          # Also create a Hash { format: path } as absolute url
          [ f.format, config.fetch('baseurl', '') + '/' + file ]
        end]

        # Copy the covers to the final site
        if book.cover?
          # Where in the metadata we want to store the path to cover
          cover_field = config.dig('calibre', 'cover', 'field') || 'cover'
          # The path is the absolute URL
          post.data[cover_field] = "#{config.fetch('baseurl', '')}/#{cover_dir}/#{book.cover}"

          # The cover is stored in a path mimicking the path from
          # calibre
          # TODO maybe use the slug or something, calibre uses spaces in
          # directories :(
          post_dir   = File.join(dest, cover_dir, book.path)
          # Original cover
          cover      = File.join(calibre_dir, book.cover)
          # Final cover
          dest_cover = File.join(post_dir, 'cover.jpg')

          # Create the directory and copy the file
          FileUtils.mkdir_p(post_dir)
          FileUtils.cp(cover, dest_cover)

          # Tell Jekyll to keep the files once finishes building
          # keep_files needs the relative path
          self.keep_files << File.join(cover_dir, book.cover)
        end

        # Add the book to the _posts collection
        posts.docs << post
      end
    end
  end
end
