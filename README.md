# jekyll-calibre

This Jekyll plugin creates a site from a Calibre library.  So it's able
to turn *any* Jekyll theme into a library!

It creates a post for every book in a Calibre library, by mapping the
book's metadata into a post metadata.  Then it copies the cover and the
files into the final site and also makes them available as metadata, so
the site can have download buttons.

## Metadata / Frontmatter

The plugin will generate Jekyll documents as if they were files in the
`_posts` collection (it's on the `TODO` list to be able to configure the
collection).

If they were written down as actual files, they would end up like this:

```
---
authors: [ Author, Author ]
title: Book title
slug: book-title
date: (Date the book was added to the library)
cover: /baseurl/absolute/url/to/book/cover.jpg
files:
  epub: /baseurl/absolute/url/to/book.epub
  pdf: /baseurl/absolute/url/to/book.pdf
pubdate: (Publication date according to book)
publisher: [ Publisher ]
---

Book comments
```

## Configuration

The plugin accepts these options in `_config.yml`:

```yaml
calibre:
  dir: '/absolute/path/to/calibre/library'
  layout: default-layout-for-books
  files:
    dir: base/directory/where/files/are/stored/in/site
  cover:
    field: field-in-front-matter-where-covers-are-stored
    dir: base/directory/where/covers/are/stored/in/site
```

* `calibre` is the base for this plugin configuration
* `dir` is the Calibre library directory, it's probably in your home.
* `layout` must correspond to a file in `_layouts` and is the template
  for each book.
* `files` are options related to files
* `dir` inside `files` is the directory where files are copied into
* `cover` are options related to covers
* `dir` inside `cover` is the directory where covers are copied into
* `field` is the frontmatter field where the theme expects to find
  cover images

The only required option is the dir where the library is stored, the
default options are:

```yaml
calibre:
  layout: post
  files:
    dir: files
  cover:
    dir: covers
    field: cover
```

