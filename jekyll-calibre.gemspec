# encoding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jekyll/calibre/version'

Gem::Specification.new do |gem|
  gem.name          = 'jekyll-calibre'
  gem.version       = Jekyll::Calibre::VERSION
  gem.authors       = ['fauno']
  gem.email         = ['fauno@partidopirata.com.ar']
  gem.description   = %q{Jekyll plugin to generate a site from a Calibre library}
  gem.summary       = %q{Jekyll plugin to generate a site from a Calibre library}
  gem.homepage      = 'https://0xacab.org/utopias-piratas/jekyll-calibre'
  gem.license       = 'MIT'

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ['lib']

  gem.add_dependency('calibre-ruby', '~> 0')
  gem.add_dependency('jekyll', '~> 3.8.0')
end
