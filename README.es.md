# jekyll-calibre

Este plugin genera un sitio Jekyll a partir de una biblioteca de
Calibre, de forma que cualquier plantilla de Jekyll pude convertirse en
una biblioteca :)

Crea un artículo por cada libro en la biblioteca de Calibre,
convirtiendo los metadatos de Calibre en metadatos de Jekyll.  Además
copia la cubierta y los archivos en el sitio final y los incorpora al
artículo como metadatos, para poder descargarlos.

## Metadatos / "Frontmatter"

El plugin genera documentos de Jekyll como si fuesen archivos de la
colección `_posts` (está pendiente poder configurar la colección).

Si fueran escritos como archivos, quedarían así:

```
---
authors: [ Autorx, Autorx ]
title: Título del libro
slug: titulo-del-libro
date: (Fecha en que el libro se agregó a la biblioteca)
cover: /baseurl/url/absoluta/a/la/cubierta.jpg
files:
  epub: /baseurl/url/absoluta/al/libro.epub
  pdf:  /baseurl/url/absoluta/al/libro.pdf
pubdate: (Fecha de publicación del libro)
publisher: [ Editora ]
---

Comentarios del libro / Reseña
```

## Configuración

El plugin acepta estas opciones en `_config.yml`:

```yaml
calibre:
  dir: '/ruta/absoluta/a/la/biblioteca/de/calibre'
  layout: plantilla
  files:
    dir: directorio/base/donde/se/copian/los/libros
  cover:
    field: campo-del-frontmatter-donde-van-las-cubiertas
    dir: directorio/base/donde/se/copian/las/cubiertas
```

* `calibre` es la base de la configuración del plugin
* `dir` es el directorio donde se encuentra la biblioteca de Calibre,
  probablemente en tu `~`
* `layout` debe corresponder a un archivo dentro de `_layouts` y es la
  plantilla para cada libro.
* `files` son las opciones relacionadas con archivos.
* `dir` dentro de `files` es el directorio donde se copian los archivos.
* `cover` son las opciones relacionadas con cubiertas.
* `dir` dentro de `cover` es el directorio donde se copian las
  cubiertas.
* `field` es el campo del _frontmatter_ donde la plantilla espera
  encontrar las imágenes de cubierta.

La única opción requerida es el directorio donde se encuentra la
biblioteca.  Las opciones por defecto son:

```yaml
calibre:
  layout: post
  files:
    dir: files
  cover:
    dir: covers
    field: cover
```
